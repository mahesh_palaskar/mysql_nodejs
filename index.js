const express = require("express");

const mysql = require("mysql");

const bodyParser = require('body-parser');

const database = mysql.createConnection({

  host: "127.0.0.1",

  user: "root",

  password: "new_password",

  database: "employee_management"

});

database.connect((err) => {
  if (err) {
    console.log("Error " + err);
  }
  console.log("MySql Connected");
});

const app = express();

app.use(bodyParser.json());

app.get("/", (req, res) => {
  let sql = "SELECT * FROM `employee`";
  database.query(sql, (err, results) => {
    if (err) {
      res.json(err);
    }
    else {
      res.json(results);
    }
  });
});

// Inserting employee 1
app.post('/', (req, res) => {
  const user= req.body;
  database.query("INSERT INTO employee SET ?",user,
    (err, rows, fields) => {
      if (err) {
        res.json(err);
      }
      else {
        res.json(rows);
      }
    });
})

//updating the details of employees
app.post("/", (req, res) => {
  console.log(req.params);
  let query = database.query("UPDATE employee SET first_name=?,last_name=? WHERE first_name=?",[req.body.first_name,req.params.last_name,req.params.prev], (err,results) => {

    if (err) {

      console.log("Error " + err);

    }

    res.json(results);

  });

});


// Delete employee

app.delete("/", (req, res) => {

  let query = database.query("DELETE FROM employee WHERE first_name=?",[req.body.first_name], (err,results) => {

    if (err) {

      console.log("Error " + err);

    }

    res.json(results);

  });

});

const port = 6002;

app.listen(port, () => {

  console.log("Server started ");

});
